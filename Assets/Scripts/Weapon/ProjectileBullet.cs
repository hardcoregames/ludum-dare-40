﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBullet : MonoBehaviour
{
    public string Tag;
    public int Damage;
    // Radius поражения
    public float LifeTime;
    public float Radius;
    public float ExplosionRadius;
    public float FireDelay;

    public ParticleSystem Impact;
    
    private Transform _transform;
    private Collision _collision;
    private bool _isTimer;

	// Use this for initialization
	void Start()
	{
	    _transform = transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        _collision = collision;
        var delay = FireDelay;
        var colliders = Physics.OverlapSphere(_transform.position, Radius);
        var enemyCollide = false;
        for (int i = 0; i < colliders.Length; i++)
        {
            var col = colliders[i];
            if (col.transform.tag == Tag)
            {
                Debug.Log("DetectEnemy");
                enemyCollide = true;
            }

            if (Tag == "Enemy" && col.transform.tag == (Tag + "Bullet"))
            {
                Debug.Log("Detect EnemyBullet");
                enemyCollide = true;
            }
        }

        // WTF O__o jam code is so jam
        if (enemyCollide)
        {
            delay = 0;
            Explosion();
        }

        if (delay > 0)
        {
            if (!_isTimer)
            {
                StartCoroutine("ExplosionLate");
                Debug.Log("Delayed explosion Start timer");
            }
            _isTimer = true;
        }
        else
        {
            Explosion();
        }
        Debug.Log("Explosion Delay" + delay);

    }

    private void Explosion()
    {
        Debug.Log("Explosion");
        var colliders = Physics.OverlapSphere(_transform.position, ExplosionRadius);
        for (int i = 0; i < colliders.Length; i++)
        {
            var col = colliders[i];
            if (col.transform.tag == Tag)
            {
                Debug.Log("Explode Late + Enemy found");

                col.gameObject.SendMessage("RemoveHealth", Damage);
            }

            if (Tag == "Enemy" && col.transform.tag == (Tag + "Bullet"))
            {
                Debug.Log("Explode Late + EnemyBullet found");

                var bullet = col.transform.GetComponent<ProjectileBullet>();
                bullet.Explode();
            }
        }
        var impact = Instantiate(Impact, _collision.contacts[0].point, Quaternion.LookRotation(_collision.contacts[0].normal));
        Destroy(impact.gameObject, 1f);
        Destroy(gameObject);

    }

    private IEnumerator ExplosionLate()
    {
        yield return new WaitForSeconds(FireDelay);
        Debug.Log("LaterExplosion");
        Explosion();

    }

    public void Explode()
    {
        var impact = Instantiate(Impact, _transform.position, Quaternion.LookRotation(_transform.position));
        Destroy(impact.gameObject, 1f);
        Destroy(gameObject);
    }
}
