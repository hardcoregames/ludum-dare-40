﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public enum EnemyType
    {
        Disable,
        Static,
        Laser,
        Flying,
        Bomb,
    }

    public EnemyType Type;

    public int Health;
    public float AttackSpeed;
    public Transform FirePoint;
    public float NextFire;
    public GameObject Bullet;
    public float BulletSpeed;
    public float LookRadius;
    public float ExplosionRadius;
    public ParticleSystem ExplosionEffect;
    public int Damage;
    public float BulletLifeTime;
    private PlayerController _player;
    private Transform _transform;
    private LineRenderer _lineRenderer;

	// Use this for initialization
	void Start ()
	{
	    _transform = transform;
	    _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	    if (Type == EnemyType.Laser) _lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        EnemyUpdateByType();
	}

    private void EnemyUpdateByType()
    {
        switch (Type)
        {
            case EnemyType.Disable:
                break;
            case EnemyType.Static:

                LookRaycast();

                break;
            case EnemyType.Laser:
                LaserSight();

                break;
            case EnemyType.Flying:
                LookRaycast();

                break;
            case EnemyType.Bomb:
                BombUpdate();
                break;
        }
    }

    public void BombUpdate()
    {
        if (Vector3.Distance(_transform.position, _player.transform.position) < LookRadius)
        {
            Explosion();
        }
    }

    public void LaserSight()
    {
        if (!CanAttack()) return;
        _lineRenderer.SetPosition(0, _transform.position);
        _lineRenderer.SetPosition(1, _transform.position + _transform.forward * LookRadius);
        NextFire = Time.time + AttackSpeed;
        RaycastHit hit;
        if (Physics.Raycast(_transform.transform.position, _transform.transform.forward, out hit, LookRadius))
        {
            _lineRenderer.SetPosition(1, hit.point);

            if (hit.transform.tag == "Player")
            {
                hit.transform.gameObject.SendMessage("RemoveHealth", Damage);
                var impact = Instantiate(ExplosionEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impact.gameObject, 1f);
            }
        }
    }

    public void LookRaycast()
    {
        if (Vector3.Distance(_transform.position, _player.transform.position) > LookRadius)
        {
            _transform.rotation = Quaternion.identity;
            return;
        }

        transform.LookAt(_player.transform);

        RaycastHit hit;
        if (Physics.Raycast(_transform.position, _transform.forward, out hit, LookRadius))
        {
            if (hit.transform.tag == "Player")
            {
                if (CanAttack()) FireProjectile();
            }
        }
    }

    public virtual bool CanAttack()
    {
        return Time.time > NextFire;
    }

    private void FireProjectile()
    {
        NextFire = Time.time + AttackSpeed;
        var bullet = (GameObject)Instantiate(
            Bullet,
            FirePoint.position,
            FirePoint.rotation);

        bullet.GetComponent<Rigidbody>().velocity = FirePoint.forward * BulletSpeed;
        Destroy(bullet, BulletLifeTime);
    }

    private void Explosion()
    {
        var colliders = Physics.OverlapSphere(_transform.position, ExplosionRadius);
        for (int i = 0; i < colliders.Length; i++)
        {
            var col = colliders[i];
            if (col.transform.tag == "Player")
            {
                col.gameObject.SendMessage("RemoveHealth", Damage);
            }
            /*
            // Bomb
            if (Type == EnemyType.Bomb)
            {
                if (col.transform.tag == "PlayerBullet" || col.transform.tag == "EnemyBullet")
                {
                    var bullet = col.transform.GetComponent<ProjectileBullet>();
                    bullet.Explode();
                }
                else if (col.transform.tag == "Enemy")
                {
                    col.gameObject.SendMessage("RemoveHealth", Damage);
                }
            }*/
        }
        DeathAnimation();
    }

    public void RemoveHealth(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Explosion();
        }
    }

    private void DeathAnimation()
    {
        var impact = Instantiate(ExplosionEffect, _transform.position, _transform.rotation);
        Destroy(impact.gameObject, 1f);
        Destroy(gameObject);
    }
}
