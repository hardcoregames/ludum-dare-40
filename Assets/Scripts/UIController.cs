﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text HpText;
    public Text KeyText;
    public Text LivesText;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void SetHp(int hp)
    {
        HpText.text = "HP: " + hp.ToString();
    }

    public void SetKeyLevel(int keyLevel)
    {
        KeyText.text = "Keys: " + keyLevel;
    }

    public void SetLives(int lives)
    {
        LivesText.text = "Lives: " + lives;
    }
}
