﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{

    public Transform Door;
    public Transform OpenDoorPosition;
    public float Speed;
    private bool _move;
    private Transform _transform;
	void Start ()
	{
	    
	}
	
	void Update ()
    {
        if (_move && Vector3.Distance(Door.position, OpenDoorPosition.position) > 0.5)
        {
            Door.position = Vector3.Slerp(Door.position, OpenDoorPosition.position, Time.deltaTime * Speed);
        }

	}

    public void OpenDoor()
    {
       _move = true;
    }
}
