﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;

public class WeaponController : MonoBehaviour
{
    [Serializable]
    public class WeaponParameters
    {
        public float AttackSpeed;
        public float BulletSpeed;

        public int Damage;
        public int Range;

        public bool Automitic;
        public WeaponType WeaponType;
        public GameObject Bullet;
        public AudioClip ShotClip;
        public ParticleSystem Impact;
    }

    public enum WeaponType
    {
        Pistol,
        MachineGun,
        RocketLauncher,
        GrenadeLauncher,
        Laser,
        Railgun,
    }

    public List<WeaponParameters> Weapons;
    public List<ParticleSystem> MuzzleFlashes;
    public List<Material> WeaponMaterials;
    public Transform FirePoint;
    
    public float NextFire;
    
    public PlayerController PlayerController;
    public AudioSource AudioSource;
    public LineRenderer Line;

    private WeaponType _weaponType;
    private Camera _camera;
    private WeaponParameters _weaponParameters;
    private ParticleSystem _muzzleFlash;
    private Renderer _renderer;

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
        Line = GetComponent<LineRenderer>();
    }

	void Start () 
    {
		_camera = Camera.main;
	    foreach (var muzzle in MuzzleFlashes)
	    {
	        muzzle.gameObject.SetActive(false);
	    }
	    Line.enabled = false;
        SetWeapon(WeaponType.Pistol);
	    Cursor.lockState = CursorLockMode.Locked;
	    Cursor.visible = false;
    }

    private void SetWeapon(WeaponType type)
    {
        FireEndByType();
        _weaponType = type;
        if (_muzzleFlash != null) _muzzleFlash.gameObject.SetActive(false);
        _renderer.material.color = WeaponMaterials[(int)_weaponType].color;
        _muzzleFlash = MuzzleFlashes[(int)_weaponType];
        _muzzleFlash.gameObject.SetActive(true);
	    _weaponParameters = Weapons[(int) _weaponType];
	    AudioSource.clip = _weaponParameters.ShotClip;
   }
	
	// Update is called once per frame
	void Update () 
    {
	    if (Input.GetButton("Fire1"))
	    {
	        FireByType();
	    }

	    if (Input.GetButtonUp("Fire1"))
	    {
	        FireEndByType();
	    }

	    //WeaponSwitcherByKey();
        WeaponSwitcherByHp();
    }

    private void FireByType()
    {
        switch (_weaponType)
        {
            case WeaponType.Pistol:
                FireRaycast();
                break;
            case WeaponType.MachineGun:
                FireRaycast();
                break;
            case WeaponType.RocketLauncher:
                FireProjectile();
                break;            
            case WeaponType.GrenadeLauncher:
                FireGrenade();
                break;
            case WeaponType.Laser:
                FireLaser();
                break;            
            case WeaponType.Railgun:
                FireRailgun();
                break;
        }
    }

    private void FireEndByType()
    {
        switch (_weaponType)
        {
            case WeaponType.Pistol:
                break;
            case WeaponType.MachineGun:
                break;
            case WeaponType.RocketLauncher:
                break;
            case WeaponType.GrenadeLauncher:
                break;
            case WeaponType.Laser:
            {
                Line.enabled = false;
                if (AudioSource.isPlaying)
                {
                    AudioSource.loop = false;
                    AudioSource.Stop();
                }
            }
                break;
            case WeaponType.Railgun:
                break;
                
        }
    }
    public virtual bool CanAttack()
    {
        return Time.time > NextFire;
    }

    private void FireRailgun()
    {
        if (!CanAttack()) return;
        NextFire = Time.time + _weaponParameters.AttackSpeed;
        AudioSource.Play();
        RaycastHit hit;
        var endPosition = FirePoint.transform.position + FirePoint.transform.forward * _weaponParameters.Range;
        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit, _weaponParameters.Range))
        {
            endPosition = hit.point;
            if (hit.transform.tag == "Enemy")
            {
                hit.transform.gameObject.SendMessage("RemoveHealth", _weaponParameters.Damage);
            }

            if (hit.transform.tag == "EnemyBullet")
            {
                var pb = hit.transform.GetComponent<ProjectileBullet>();
                pb.Explode();
            }

            var impact = Instantiate(_weaponParameters.Impact, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impact.gameObject, 1f);
        }
        var ray = Instantiate(_weaponParameters.Bullet, _camera.transform.forward, _camera.transform.rotation);
        var line = ray.GetComponent<LineRenderer>();
        line.SetPosition(0, FirePoint.transform.position);
        line.SetPosition(1, endPosition);
        Destroy(ray.gameObject, 1f);
    }

    private void FireLaser()
    {
        if (!Line.enabled) Line.enabled = true;
        if (!AudioSource.isPlaying)
        {
            AudioSource.loop = true;
            AudioSource.Play();
        } 
        Line.SetPosition(0, FirePoint.transform.position);
        RaycastHit hit;
        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit, _weaponParameters.Range))
        {
            if (CanAttack() && hit.collider)
            {
                Line.SetPosition(1, hit.point);
                if (hit.transform.tag == "Enemy")
                {
                    hit.transform.gameObject.SendMessage("RemoveHealth", _weaponParameters.Damage);
                }

                if (hit.transform.tag == "EnemyBullet")
                {
                    var pb = hit.transform.GetComponent<ProjectileBullet>();
                    pb.Explode();
                }

                var impact = Instantiate(_weaponParameters.Impact, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impact.gameObject, 0.2f);
            }
        }
        else
        {
            Line.SetPosition(1, FirePoint.transform.position + FirePoint.forward * _weaponParameters.Range);
        }
    }

    private void DrawLaser()
    {

    }

    private void FireRaycast()
    {
        if (!CanAttack()) return;
        AudioSource.pitch = (Random.Range(0.6f, .9f));

        AudioSource.Play();

        _muzzleFlash.Play();
        NextFire = Time.time + _weaponParameters.AttackSpeed;
        RaycastHit hit;
        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit, _weaponParameters.Range))
        {
            
            if (hit.transform.tag == "Enemy")
            {
                hit.transform.gameObject.SendMessage("RemoveHealth", _weaponParameters.Damage);
            }

            if (hit.transform.tag == "EnemyBullet")
            {
                var pb = hit.transform.GetComponent<ProjectileBullet>();
                pb.Explode();
            }

            var impact = Instantiate(_weaponParameters.Impact, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impact.gameObject, 1f);
        }
    }

    private void FireProjectile()
    {
        if (!CanAttack()) return;
        AudioSource.pitch = (Random.Range(0.6f, .9f));

        AudioSource.Play();
        _muzzleFlash.Play();
        var pos = (_camera.transform.position + _camera.transform.forward);
        NextFire = Time.time + _weaponParameters.AttackSpeed;
        var bullet = (GameObject)Instantiate(
            _weaponParameters.Bullet,
            pos,
            FirePoint.rotation);

        bullet.GetComponent<Rigidbody>().velocity = _camera.transform.forward * _weaponParameters.BulletSpeed;
        Destroy(bullet, 3f);        
    }

    private void FireGrenade()
    {
        if (!CanAttack()) return;
        AudioSource.pitch = (Random.Range(0.6f, .9f));
        AudioSource.Play();
        _muzzleFlash.Play();
        var pos = (_camera.transform.position + _camera.transform.forward);
        NextFire = Time.time + _weaponParameters.AttackSpeed;
        var bullet = (GameObject)Instantiate(
            _weaponParameters.Bullet,
            pos,
            FirePoint.rotation);
        bullet.GetComponent<Rigidbody>().AddForce((_camera.transform.forward + Vector3.up) * _weaponParameters.BulletSpeed, ForceMode.Impulse);
        //bullet.GetComponent<Rigidbody>().velocity = _camera.transform.forward * _weaponParameters.BulletSpeed;
        Destroy(bullet, 10f);  
    }

    #region Weapon Switcher
    private void WeaponSwitcherByKey()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetWeapon(WeaponType.Pistol);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetWeapon(WeaponType.MachineGun);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SetWeapon(WeaponType.RocketLauncher);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SetWeapon(WeaponType.GrenadeLauncher);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SetWeapon(WeaponType.Laser);
        }       
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SetWeapon(WeaponType.Railgun);
        }
    }

    public void WeaponSwitcherByHp()
    {
        // Hardcode
        if (PlayerController.Health >= 101 && _weaponType != WeaponType.GrenadeLauncher)
        {
            SetWeapon(WeaponType.GrenadeLauncher);
        }

        else if (PlayerController.Health <= 100 && PlayerController.Health >= 80 && _weaponType != WeaponType.Pistol)
        {
            SetWeapon(WeaponType.Pistol);
        }  
      
        else if (PlayerController.Health < 80 && PlayerController.Health >= 60 && _weaponType != WeaponType.MachineGun)
        {
            SetWeapon(WeaponType.MachineGun);
        }    
    
        else if (PlayerController.Health < 60 && PlayerController.Health >= 50 && _weaponType != WeaponType.Railgun)
        {
            SetWeapon(WeaponType.Railgun);
        }

        else if (PlayerController.Health < 50 && PlayerController.Health >= 30 && _weaponType != WeaponType.Laser)
        {
            SetWeapon(WeaponType.Laser);
        }
        else if (PlayerController.Health < 30 && _weaponType != WeaponType.RocketLauncher)
        {
            SetWeapon(WeaponType.RocketLauncher);
        }
    }
    #endregion
}
