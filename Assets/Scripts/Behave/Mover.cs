﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour 
{

    public List<Transform> Positions;
    public float Speed;
    public bool PingPong;
    public bool Loop;
    private Transform _transform;
    private Vector3 _nextPosition;
    private int _moveCount;

	void Start()
	{
	    _transform = transform;
        _moveCount = 1;
	}
	
	void Update() 
    {
        //_transform.position = Vector3.MoveTowards(_transform.position, Positions[_moveCount].position, Time.deltaTime * Speed);
        _transform.position = Vector3.Slerp(_transform.position, Positions[_moveCount].position, Time.deltaTime * Speed);
        
        if (Vector3.Distance(_transform.position, Positions[_moveCount].position) <= 0.5) 
        {
            _moveCount++;
            if (_moveCount == Positions.Count) _moveCount = 0;
            _nextPosition = Positions[_moveCount].position;
        }
    }
}
