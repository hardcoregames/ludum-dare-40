﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRPlayer;

public class PlayerController : MonoBehaviour
{
    public int Health = 100;
    public int KeyLevel;
    public int Lives = 3;
    public Transform SpawnPosition;
    public AudioClip HurtClip;
    public AudioClip TakeKeyClip;
    public AudioClip AddHealthClip;

    private CharacterController _characterController;
    private MovementController _movementController;
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _movementController = GetComponent<MovementController>();
        _characterController = GetComponent<CharacterController>();
    }

	void Start() 
    {
        GameManager.Instance.UIController.SetHp(Health);
	}
	
	void Update() 
    {
	}

    void TakeItem(Item.ItemType item)
    {
        switch (item)
        {
            case Item.ItemType.HealthPlus:
                AddHealth(20);
                break;
            case Item.ItemType.HealthMinus:
                RemoveHealth(10);                
                break;
            case Item.ItemType.Key:
            {
                _audioSource.clip = TakeKeyClip;
                _audioSource.Play();
                KeyLevel++;

            }
                break;
        }
        GameManager.Instance.UIController.SetHp(Health);
        GameManager.Instance.UIController.SetKeyLevel(KeyLevel);
    }

    public void AddHealth(int add)
    {
        Health += add;
        if (Health > 150)
        {
            Health = 150;
        }
        _audioSource.clip = AddHealthClip;
        _audioSource.Play();
        // TO DO
        // Сделать макс хп которое уменьшается
    }

    public void RemoveHealth(int damage)
    {
        Health -= damage;
        GameManager.Instance.UIController.SetHp(Health);
        _audioSource.clip = HurtClip;
        _audioSource.Play();
        if (Health <= 0)
        {
            Ressurect();
        }
    }

    public void Ressurect()
    {
        Lives--;
        if (Lives == 0)
        {
            GameManager.Instance.RestartLevel();
        } 
        transform.position = SpawnPosition.position;
        Health = 100;
        GameManager.Instance.UIController.SetHp(Health);
        GameManager.Instance.UIController.SetLives(Lives);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Item")
        {
            var item = other.transform.GetComponent<Item>();
            TakeItem(item.Type);
            Destroy(other.gameObject);
        }

        if (other.transform.tag == "Door")
        {
            if (KeyLevel == 6)
            {
                var item = other.transform.GetComponent<DoorTrigger>();
                item.OpenDoor();
            }
        }

        if (other.transform.tag == "Jumper")
        {
            _movementController.Jump(10);
        }
    }

    void OnCollisionEnter(Collision collision)
    {

    }
}
