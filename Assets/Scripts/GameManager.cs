﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public UIController UIController;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)           
            Destroy(gameObject);
        InitGame();
    }

    void InitGame()
    {
        UIController = GetComponent<UIController>();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}
}
